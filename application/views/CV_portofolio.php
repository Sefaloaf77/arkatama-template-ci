<!DOCTYPE html>
<html>

<head>
	<title>CV Portofolio</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!-- icon -->
	<script src="https://kit.fontawesome.com/244c1245b5.js" crossorigin="anonymous"></script>
	<link rel="stylesheet" href="https://fonts.google.com/specimen/Big+Shoulders+Display">
	<style>
		body {
			/* width: 70%; */
			justify-content: center;
			margin: auto;
			font-family: Verdana, Geneva, Tahoma, sans-serif;
			/* background-color: rgb(208, 209, 209); */
		}

		h3 {
			background-color: #FCA311;
			padding: 10px;
			color: #14213D;
		}

		hr {
			margin-top: -10px;
		}
	</style>
</head>

<body>
	<section class="top" style="width: 100%; background-color: #14213D;height: 250px;">
		<img src="<?= base_url('assets/img/') . $cv_data[3]['value'] ?>" alt="gambar profil" style="width: 20%; margin-top: 70px; margin-left: 30px;">
		<div style="color: #eaeaea; padding: 5px 20px; text-align: left; width: 60%; position: absolute; top:-8px; ">
			<h2 style="color: white;"><?= $cv_data[0]['value'] ?> </h2>
			<p style="opacity:70%;"><?= $cv_data[1]['value'] ?></p>
			<p><?= $cv_data[2]['value'] ?></p>
		</div>
	</section>
	<section>
		<section class="left" style="float:left; width: 28%; background-color: #14213D; color: #eaeaea; padding: 20px;">
			<div class="sosmed">
				<h3>Social Media</h3>
				<?php foreach ($social_medias as $sosmed) { ?>
					<div class="social medias" style="margin-left: 15px; margin-bottom: 10px;">
						<img src="<?= base_url('assets/img/sosmedImage/') . $sosmed->icon ?>" alt="" style="width: 16px; height: 16px;">
						<i class="" style="font-size: 15px; margin-right: 5px;"></i>
						<a href="<?= $sosmed->url ?>" style="color: white; text-decoration: none;"><?= $sosmed->name ?></a>
					</div>
				<?php } ?>

				<div class="hobby">
					<h3>Hobbies</h3>

					<table>
						<tbody>
							<?php foreach ($hobbies as $hobby) { ?>
								<tr>
									<td>
										<li style="margin-left: 20px"><?= $hobby->hobby ?></li>
									</td>
								</tr>
							<?php } ?>
						</tbody>
					</table>
				</div>
				<div class="language">
					<h3>Language</h3>

					<table>
						<tbody>
							<?php foreach ($languages as $language) { ?>
								<tr>
									<td>
										<li style="margin-right: 8px;margin-left: 20px"><?= $language->name ?></li>
									</td>
									<td>
										<div class="progress" style="background-color: white; height: 5px; width: 100px; border-radius: 5px;">
											<div class="progress-bar" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="background-color: #FCA311; height: 5px; border-radius: 5px; width: <?php
																																																							$total = 100;
																																																							$progress = $language->level;
																																																							$panjang = ($progress * 100) / 5;
																																																							echo $panjang; ?>%;">
											</div>
										</div>
									</td>
								</tr>
							<?php } ?>
						</tbody>
					</table>
				</div>
				<div class="skills">
					<h3>Skills</h3>
					<table>
						<tbody>
							<?php foreach ($skills as $skill) { ?>
								<tr>
									<td>
										<li style="margin-right: 30px;margin-left: 20px"><?= $skill->name ?></li>
									</td>
									<td>
										<div class="progress" style="background-color: white; height: 5px; width: 100px; border-radius: 5px;">
											<div class="progress-bar" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="background-color: #FCA311; height: 5px; border-radius: 5px; width: <?php
																																																							$total = 100;
																																																							$progress = $skill->level;
																																																							$panjang = ($progress * 100) / 5;
																																																							echo $panjang;
																																																							?>%;"></div>
										</div>
									</td>
								</tr>

							<?php } ?>
						</tbody>
					</table>
				</div>
		</section>
		<section class="right" style="float: right; width: 60%;margin-left: 20px; padding: 5px 20px; background-color: white;">
			<div class="education">
				<h3>Education</h3>

				<?php foreach ($educations as $education) { ?>
					<div class="schools">
						<div class="name-year">
							<span style="font-weight: bold;">> <?= $education->name ?></span>
							<span style="margin-left: 40px;background-color: #14213D;padding: 5px; color:white;"><?= $education->start_year ?> - <?= $education->graduated_date ?></span>
						</div>
						<p><?= $education->description ?></p>
					</div>
				<?php } ?>

			</div>
			<div class="ecperience">
				<h3>Experience</h3>
				<?php foreach ($experiences as $experience) { ?>
					<div class="office">
						<div class="name-year">
							<span style="font-weight: bold;">> <?= $experience->name ?></span>
							<span style="margin-left: 40px; background-color: #14213D;padding: 5px; color:white;"><?= $experience->start_year ?> - <?= $experience->resign_date ?></span>
						</div>
						<p><?= $experience->description ?></p>
					</div>
				<?php } ?>
			</div>
		</section>
	</section>
</body>

</html>
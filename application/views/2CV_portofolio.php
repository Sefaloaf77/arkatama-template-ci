<!DOCTYPE html>
<html><head>
	<title>CV Portofolio</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!-- icon -->
	<script src="https://kit.fontawesome.com/244c1245b5.js" crossorigin="anonymous"></script>
	<link rel="stylesheet" href="https://fonts.google.com/specimen/Big+Shoulders+Display">
	<style>
		body {
			justify-content: center;
			background-color: rgb(208, 209, 209);
		}
		hr {
			margin-top: -10px;
		}
	</style>
</head><body>
	<section class="top" style="width: 100%; background-color: rgb(31, 25, 25);display: flex;justify-content: center;">
		<!-- <img src="../../assets/img/profil.png" alt=""> -->
		<div style="color: #eaeaea; padding: 5px 20px; text-align: center; width: 60%;">
			<h2><span>Sefalo</span> 'Aadila Faruq</h2>
			<p style="opacity:70%;">Web Developer</p>
			<p>Lorem ipsum dolor sit amet consectetur, adipisicing elit. Laboriosam alias sequi fuga culpa temporibus
				unde iste?
				Recusandae vitae at fuga sit blanditiis, dolore provident nihil velit explicabo consectetur unde amet.
			</p>
		</div>
	</section>
	<section>
		<section class="left" style="flex: 2; background-color: white; padding: 20px;">
			<div class="sosmed">
				<h3>Social Media</h3>
				<hr>
				<div class="ig">
					<i class="fa-brands fa-instagram"></i>
					<span>@Sefalo.af</span>
				</div>
				<div class="twt" style="margin-top: 10px">
					<i class="fa-brands fa-twitter"></i>
					<span>sefaloaf</span>
				</div>
				<div class="email" style="margin-top: 10px">
					<i class="fa-regular fa-envelope"></i>
					<span>sefaloaf@gmail.com</span>
				</div>
			</div>
			<div class="hobby">
				<h3>Hobbies</h3>
				<hr>
				<table>
					<tbody>
						<tr>
							<td>
								<li>Basketball</li>
							</td>
						</tr>
						<tr>
							<td>
								<li>Photography</li>
							</td>
						</tr>
						<tr>
							<td>
								<li>Videography</li>
							</td>
						</tr>
						<tr>
							<td>
								<li>Videography</li>
							</td>
						</tr>
						<tr>
							<td>
								<li>Playing Game</li>
							</td>
						</tr>
					</tbody>
				</table>
			</div>
			<div class="language">
				<h3>Language</h3>
				<hr>
				<table>
					<tbody>
						<tr>
							<td>
								<li style="margin-right: 30px;">Indonesian</li>
							</td>
							<td style="margin-left: 30px;">5</td>
						</tr>
						<tr>
							<td>
								<li style="margin-right: 30px;">English</li>
							</td>
							<td style="margin-left: 30px;">5</td>
						</tr>
						<tr>
							<td>
								<li style="margin-right: 30px;">Java</li>
							</td>
							<td style="margin-left: 30px;">5</td>
						</tr>
					</tbody>
				</table>
			</div>
			<div class="skills">
				<h3>Skills</h3>
				<hr>
				<table>
					<tbody>
						<tr>
							<td>
								<li style="margin-right: 30px;">HTML</li>
							</td>
							<td style="margin-left: 30px;">5</td>
						</tr>
						<tr>
							<td>
								<li style="margin-right: 30px;">CSS</li>
							</td>
							<td style="margin-left: 30px;">5</td>
						</tr>
						<tr>
							<td>
								<li style="margin-right: 30px;">PHP</li>
							</td>
							<td style="margin-left: 30px;">5</td>
						</tr>
						<tr>
							<td>
								<li style="margin-right: 30px;">UI/UX</li>
							</td>
							<td style="margin-left: 30px;">5</td>
						</tr>
					</tbody>
				</table>
			</div>
		</section>
		<section class="right" style="flex: 4; padding: 5px 20px; background-color: white;">
			<div class="education">
				<h3>Education</h3>
				<hr>
				<div class="man">
					<div class="name-year" style="display: flex; justify-content: space-between;">
						<span style="font-weight: bold;">🔴 MAN 2 Tulungagung</span>
						<span>2017-2020</span>
					</div>
					<p>Lorem, ipsum dolor sit amet consectetur adipisicing elit. Ad ea amet laboriosam fuga quisquam
						quidem magni perferendis illum, dolor possimus labore voluptatibus veniam quo est aspernatur
						sequi! Fuga itaque non beatae qui reiciendis aliquid, placeat alias libero laboriosam ipsum
						facere harum, quibusdam atque eveniet eius sunt! Placeat voluptatum quam distinctio?</p>
				</div>
				<div class="college">
					<div class="name-year" style="display: flex; justify-content: space-between;">
						<span style="font-weight: bold;">🔴 Universitas Brawijaya</span>
						<span>2020- now</span>
					</div>
					<p>Lorem, ipsum dolor sit amet consectetur adipisicing elit. Ad ea amet laboriosam fuga quisquam
						quidem magni perferendis illum, dolor possimus labore voluptatibus veniam quo est aspernatur
						sequi! Fuga itaque non beatae qui reiciendis aliquid, placeat alias libero laboriosam ipsum
						facere harum, quibusdam atque eveniet eius sunt! Placeat voluptatum quam distinctio?</p>
				</div>
			</div>
			<div class="ecperience">
				<h3>Experience</h3>
				<hr>
				<div class="man">
					<div class="name-year" style="display: flex; justify-content: space-between;">
						<span style="font-weight: bold;">🔸 MAN 2 Tulungagung</span>
						<span>2017-2020</span>
					</div>
					<p>Lorem, ipsum dolor sit amet consectetur adipisicing elit. Ad ea amet laboriosam fuga quisquam
						quidem magni perferendis illum, dolor possimus labore voluptatibus veniam quo est aspernatur
						sequi! Fuga itaque non beatae qui reiciendis aliquid, placeat alias libero laboriosam ipsum
						facere harum, quibusdam atque eveniet eius sunt! Placeat voluptatum quam distinctio?</p>
				</div>
				<div class="college">
					<div class="name-year" style="display: flex; justify-content: space-between;">
						<span style="font-weight: bold;">🔸 Universitas Brawijaya</span>
						<span>2020- now</span>
					</div>
					<p>Lorem, ipsum dolor sit amet consectetur adipisicing elit. Ad ea amet laboriosam fuga quisquam
						quidem magni perferendis illum, dolor possimus labore voluptatibus veniam quo est aspernatur
						sequi! Fuga itaque non beatae qui reiciendis aliquid, placeat alias libero laboriosam ipsum
						facere harum, quibusdam atque eveniet eius sunt! Placeat voluptatum quam distinctio?</p>
				</div>
			</div>
		</section>


	</section>
</body></html>
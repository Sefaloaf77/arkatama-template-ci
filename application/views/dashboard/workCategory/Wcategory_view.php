        <div class="container-fluid py-4">
            <div class="row my-4">
                <div class="col-lg-12 mb-md-0 mb-4">
                    <div class="card">
                        <div class="card-header pb-0">
                            <div class="row">
                                <div class="col-lg-6 col-7">
                                    <h6>Work Category</h6>
                                </div>
                                <div class="col-lg-6 col-3 my-auto text-end">
                                    <a class="btn bg-gradient-dark mb-0" href="<?= base_url('DworkCategory/addView') ?>">Add New Work Category</a>
                                </div>
                            </div>
                        </div>
                        <div class="card-body px-2 pb-2">
                            <div class="table-responsive">
                                <table class="table align-items-center mb-0 display" id="table_id">
                                    <thead>
                                        <tr>
                                            <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">No.</th>
                                            <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">Work Id</th>
                                            <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-2">Category Id</th>
                                            <!-- <th class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">Action</th> -->
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <?php
                                            $no = 0;
                                            foreach ($workCategories as $row) {
                                                $no += 1;
                                            ?>
                                                <td>
                                                    <span class="text-xs font-weight-bold ms-3"><?= $no ?></span>
                                                </td>
                                                <td>
                                                    <span class="text-xs font-weight-bold ms-3"><?= $row->work_id ?></span>
                                                </td>
                                                <td>
                                                    <span class="text-xs font-weight-bold ms-3"><?= $row->category_id ?></span>
                                                </td>
                                                <!-- <td class="align-middle text-center text-sm">
                                                    <div class="action-button mx-auto text-center">
                                                        <a class="btn btn-link text-dark px-3 mb-0" href="<?= base_url('DworkCategory/editView/') . $row->id ?>"><i class="fas fa-pencil-alt text-dark me-2" aria-hidden="true"></i>Edit</a>
                                                        <a class="btn btn-link text-danger text-gradient px-3 mb-0" href="<?= base_url('DworkCategory/delete/') . $row->id ?>t:;"><i class="far fa-trash-alt me-2"></i>Delete</a>
                                                    </div>
                                                </td> -->
                                        </tr>
                                    <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
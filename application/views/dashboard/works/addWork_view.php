        <div class="container-fluid py-4">
            <div class="row my-4">
                <?= $this->session->flashdata('message'); ?>
                <div class="col-lg-12 mb-md-0 mb-4">
                    <div class="card">

                        <div class="card-body px-3 pb-4">
                            <div class="table-responsive ps-3">
                                <form method="POST" action="<?= base_url('Dwork/add') ?>" role="form" enctype="multipart/form-data">
                                    <label>Work Title</label>
                                    <div class="mb-3">
                                        <input type="text" class="form-control border-danger" name="title" id="floatingInput" placeholder="Work Title" value="<?= set_value('title') ?>">
                                        <?= form_error('title', '<small class="text-danger pl-3">', '</small>'); ?>
                                    </div>
                                    <label>Year</label>
                                    <div class="mb-3">
                                        <input type="text" class="form-control border-danger" name="year" id="floatingInput" placeholder="Year" value="<?= set_value('year') ?>">
                                        <?= form_error('year', '<small class="text-danger pl-3">', '</small>'); ?>
                                    </div>
                                    <label>Content</label>
                                    <div class="mb-3">
                                        <textarea name="content" id="summernote" cols="100" rows="10" class="form-control border-danger" placeholder="Content" value="<?= set_value('content') ?>"></textarea>
                                        <?= form_error('content', '<small class="text-danger pl-3">', '</small>'); ?>
                                    </div>
                                    <label>Featured Image</label>
                                    <div class="mb-3">
                                        <input type="file" class="form-control border-danger" name="featured_image" id="floatingInput" size="20" placeholder="Featured Image" value="" accept="image/x-png, image/jpeg, image/jpg">
                                        <?= form_error('featured_image', '<small class="text-danger pl-3">', '</small>'); ?>
                                    </div>
                                    <label>Select Category</label>
                                    <div class="mb-3">
                                        <select class="form-select" name="category_id[]" aria-label="Default select example" multiple aria-label="multiple select example">
                                            <?php
                                            foreach ($category as $row) { ?>
                                                <option value="<?= $row['id'] ?>"><?= $row['name'] ?></option>
                                            <?php $i++;
                                            }
                                            ?>

                                        </select>
                                        <?= form_error('content', '<small class="text-danger pl-3">', '</small>'); ?>
                                    </div>
                                    <div class="ms-2">
                                        <button type="submit" class="btn bg-gradient-info w-25 mt-4 mb-0">Add</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
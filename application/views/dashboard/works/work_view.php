        <div class="container-fluid py-4">
            <div class="row mx-auto d-flex w-75">
                <div class="col-lg-5 col-md-12 col-sm-12 col-12">
                    <label class="text-muted">Filter Tahun</label>
                    <select class="select-year w-100" name="year" id="year">
                        <option value="">Select Year</option>
                        <?php
                        foreach ($year as $row) {
                        ?> <option value="<?= $row->year ?>"><?= $row->year ?></option>
                        <?php } ?>
                    </select>
                </div>
                <div class="col-lg-5 col-md-12 col-sm-12 col-12">
                    <label class="text-muted">Filter Kategori</label>
                    <select class="js-example-basic-multiple w-100 h-25" name="category[]" id="category" multiple="multiple">
                        <?php
                        foreach ($category as $row) { ?>
                            <option value="<?= $row['id'] ?>"><?= $row['name'] ?></option>
                        <?php
                        }
                        ?>
                    </select>
                </div>
                <div class="col-2 d-flex">
                    <button type="submit" class="btn mt-4 bg-gradient-primary" id="btn-filter">Filter</button>
                </div>
            </div>
            <div class="row my-4">
                <div class="col-lg-12 mb-md-0 mb-4">
                    <div class="card">
                        <div class="card-header pb-0">
                            <?= $this->session->flashdata('message'); ?>
                            <div class="row">
                                <div class="col-lg-6 col-7">
                                    <h6>Works</h6>
                                </div>
                                <div class="col-lg-6 col-5 my-auto align-items-center text-end">
                                    <div class="pe-4">
                                        <a class="btn bg-gradient-dark mb-0" href="<?= base_url('Dwork/addView') ?>">Add New Work</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card-body px-2 pb-2">
                            <div class="table-responsive">
                                <table class="table align-items-center mb-0 pb-2" id="table_id">
                                    <thead>
                                        <tr>
                                            <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">No</th>
                                            <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-2">Title</th>
                                            <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-2">Year</th>
                                            <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-2">Featured Image</th>
                                            <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-2">Created At</th>
                                            <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-2">Created By</th>
                                            <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-2">Updated At</th>
                                            <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-2">Updated By</th>
                                            <th class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <!-- <td class="align-middle text-center text-sm">
                                                    <div class="action-button mx-auto text-end">
                                                        <a class="btn btn-link text-dark text-decoration-none px-3 mb-0" href="<?= base_url('Dwork/editView/') . $row->id ?>"><i class="fas fa-pencil-alt text-dark me-1" aria-hidden="true"></i>Edit</a>
                                                        <a class="btn btn-link text-danger text-gradient px-3 mb-0" href="<?php echo base_url('Dwork/delete/') . $row->id ?>"><i class="far fa-trash-alt me-1"></i>Delete</a>
                                                        <a class="btn btn-link text-danger text-gradient px-3 mb-0" href="<?= base_url('Dwork/detailView/') . $row->id ?>"><i class="ni ni-tv-2 me-1"></i>Detail</a>
                                                    </div>
                                                </td> -->
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <script src="https://code.jquery.com/jquery-3.5.1.js"></script>
            <script src="https://cdn.datatables.net/1.12.1/js/jquery.dataTables.min.js"></script>
            <script>
                $(document).ready(function() {
                    var tabel = $('#table_id').DataTable({
                        processing: true,
                        serverSide: true,
                        ordering: true,
                        ajax: {
                            "url": "<?= base_url('Dwork/worksAjax') ?>",
                            "type": "POST",
                            "data": function(d) {
                                // ambil data dari select//
                                d.year = $("#year").val();
                                d.category = $("#category").val();
                                console.log(d);
                            }
                        },
                        columns: [{
                                "data": "no"
                            }, {
                                "data": "title"
                            },
                            {
                                "data": "year"
                            },
                            {
                                "data": "featured_image",
                                "render": function(data, type, row, meta) {
                                    var img = '<img src="<?= base_url('assets/img/worksImage/') ?>' + row.featured_image + '" class="img-fluid" style="height:50%" alt="">';
                                    return img;
                                }
                            },
                            {
                                "data": "created_at"
                            },
                            {
                                "data": "created_by"
                            },
                            {
                                "data": "updated_at"
                            },
                            {
                                "data": "updated_by"
                            },

                        ],
                        "columnDefs": [{
                                "targets": 8,
                                "row": "id",
                                "render": function(data, type, row, meta) {
                                    return '<div class="action-button mx-auto text-end">' +
                                        '<a href="<?= base_url('Dwork/editView/') ?>' + row.id + '" class="btn btn-link text-dark text-decoration-none px-3 mb-0"><i class="fas fa-pencil-alt text-dark me-1" aria-hidden="true"></i>Edit</a>' +
                                        '<a href="<?php echo base_url('Dwork/delete/') ?>' + row.id + '" class="btn btn-link text-danger text-gradient px-3 mb-0"><i class="fas fa-pencil-alt text-dark me-1" aria-hidden="true"></i>Delete</a>' + '<a href="<?php echo base_url('Dwork/detailView/') ?>' + row.id + '" class="btn btn-link text-danger text-gradient px-3 mb-0"><i class="ni ni-tv-2 me-1"></i>Details</a>'
                                    '</div>';
                                }
                            },
                            {
                                "targets": [0, 3, 5, 7],
                                "orderable": false
                            }
                        ],
                        "order": []
                    });
                    $("#btn-filter").click(function(e) {
                        e.preventDefault();
                        // let tahun = $("#year").val();
                        // console.log(tahun);
                        tabel.draw();
                    });
                });
            </script>
            <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
            <script>
                $(document).ready(function() {
                    $('#year').select2({});
                    // $('#year').on('select2:select', function(e) {
                    //     var data = e.params.data;
                    //     console.log(data);
                    // });
                });
            </script>
            <script>
                $(document).ready(function() {
                    $('.js-example-basic-multiple').select2();
                });
            </script>
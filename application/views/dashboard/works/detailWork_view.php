        <div class="container-fluid py-4">
            <div class="row my-4">
                <?= $this->session->flashdata('message'); ?>
                <div class="col-lg-12 col-md-8 mb-md-0 mb-4">
                    <a class="btn bg-gradient-dark mb-4 w-auto " href="<?= base_url('Dwork') ?>">Back</a>
                    <div class="card">
                        <div class="card-body px-3 pb-4">
                            <div class="table-responsive table-borderless ps-3">
                                <table class="table table-borderless">
                                    <tbody>
                                        <tr>
                                            <td class="fw-bold text-dark">Work Title</td>
                                            <td>:</td>
                                            <td><?= $work->title ?></td>
                                        </tr>
                                        <tr>
                                            <td class="fw-bold text-dark">Year</td>
                                            <td>:</td>
                                            <td><?= $work->year ?></td>
                                        </tr>
                                        <tr>
                                            <td class="fw-bold text-dark">Content</td>
                                            <td>:</td>
                                            <td><?= $work->content ?></td>
                                        </tr>
                                        <tr>
                                            <td class="fw-bold text-dark">Featured Image</td>
                                            <td>:</td>
                                            <td><img src="<?= base_url('assets/img/worksImage') . "/" . $work->featured_image ?>" class="img-fluid mt-3" alt="" style="width: 50%"></td>
                                        </tr>
                                        <tr>
                                            <td class="fw-bold text-dark">Created By</td>
                                            <td>:</td>
                                            <td><?= $work->created_by ?></td>
                                        </tr>
                                        <tr>
                                            <td class="fw-bold text-dark">Updated By</td>
                                            <td>:</td>
                                            <td><?= $work->updated_by ?></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
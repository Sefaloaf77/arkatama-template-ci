        <div class="container-fluid py-4">
            <div class="row my-4">
                <?= $this->session->flashdata('message'); ?>
                <div class="col-lg-12 col-md-8 mb-md-0 mb-4">
                    <div class="card">

                        <div class="card-body px-3 pb-4">
                            <div class="table-responsive ps-3">
                                <form method="POST" action="<?= base_url('Dwork/edit/') . $work->id ?>" role="form">
                                    <input type="hidden" name="id" value="<?php echo $work->id ?>">
                                    <input type="hidden" name="created_by" value="<?= $work->created_by ?>">
                                    <label>Work Title</label>
                                    <div class="mb-3">
                                        <input type="text" class="form-control border-danger" name="title" id="floatingInput" placeholder="Work Title" value="<?= $work->title ?>">
                                        <?= form_error('title', '<small class="text-danger pl-3">', '</small>'); ?>
                                    </div>
                                    <label>Year</label>
                                    <div class="mb-3">
                                        <input type="text" class="form-control border-danger" name="year" id="floatingInput" placeholder="Year" value="<?= $work->year ?>">
                                        <?= form_error('content', '<small class="text-danger pl-3">', '</small>'); ?>
                                    </div>
                                    <label>Content</label>
                                    <div class="mb-3">
                                        <textarea name="content" id="summernote" cols="100" rows="10" class="form-control border-danger" placeholder="Content" value=""><?= $work->content ?></textarea>
                                        <?= form_error('content', '<small class="text-danger pl-3">', '</small>'); ?>
                                    </div>
                                    <label>Featured Image</label>
                                    <div class="mb-3">
                                        <input type="file" class="form-control border-danger" name="featured_image" id="floatingInput" placeholder="Featured Image" value="<?= $work->featured_image ?>">
                                        <img src="<?= base_url('assets/img/worksImage') . "/" . $work->featured_image ?>" class="img-fluid mt-3" alt="" style="width: 20%">

                                        <input type="hidden" name="gambarLama" value="<?php echo $work->featured_image ?>">
                                        <?= form_error('featured_image', '<small class="text-danger pl-3">', '</small>'); ?>
                                    </div>
                                    
                                    <div class="ms-2">
                                        <button type="submit" class="btn bg-gradient-info w-25 mt-4 mb-0">Update</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
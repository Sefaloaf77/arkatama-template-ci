        <div class="container-fluid py-4">
            <div class="row my-4">
                <?= $this->session->flashdata('message'); ?>
                <div class="col-lg-12 col-md-8 mb-md-0 mb-4">
                    <div class="card">

                        <div class="card-body px-3 pb-4">
                            <div class="table-responsive ps-3">
                                <form method="POST" action="<?= base_url('Dpost/edit/') . $post->id ?>" role="form">
                                    <input type="hidden" name="id" value="<?php echo $post->id ?>">
                                    <input type="hidden" name="created_by" value="<?= $post->created_by ?>">

                                    <label>Post Title</label>
                                    <div class="mb-3">
                                        <input type="text" class="form-control border-danger" name="title" id="floatingInput" placeholder="Post Title" value="<?= $post->title ?>">
                                        <?= form_error('title', '<small class="text-danger pl-3">', '</small>'); ?>
                                    </div>
                                    <label>Year</label>
                                    <div class="mb-3">
                                        <input type="text" class="form-control border-danger" name="content" id="floatingInput" placeholder="Content" value="<?= $post->year ?>">
                                        <?= form_error('content', '<small class="text-danger pl-3">', '</small>'); ?>
                                    </div>
                                    <label>Content</label>
                                    <div class="mb-3">
                                        <input type="text" class="form-control border-danger" name="content" id="floatingInput" placeholder="Content" value="<?= $post->content ?>">
                                        <?= form_error('content', '<small class="text-danger pl-3">', '</small>'); ?>
                                    </div>
                                    <label>Featured Image</label>
                                    <div class="mb-3">
                                        <input type="file" class="form-control border-danger" name="featured_image" id="floatingInput" placeholder="Featured Image" value="<?= $post->featured_image ?>">
                                        <img src="<?= base_url('assets/img/postsImage') . "/" . $post->featured_image ?>" class="img-fluid mt-3" alt="" style="width: 20%">
                                        <?= form_error('featured_image', '<small class="text-danger pl-3">', '</small>'); ?>

                                        <input type="hidden" name="gambarLama" value="<?php echo $post->featured_image ?>">
                                    </div>
                                    <div class="ms-2">
                                        <button type="submit" class="btn bg-gradient-info w-25 mt-4 mb-0">Update</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
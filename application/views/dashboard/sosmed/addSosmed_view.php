        <div class="container-fluid py-4">
            <div class="row my-4">
                <?= $this->session->flashdata('message'); ?>
                <div class="col-lg-12 mb-md-0 mb-4">
                    <div class="card">

                        <div class="card-body px-3 pb-4">
                            <div class="table-responsive ps-3">
                                <form method="POST" action="<?= base_url('Dsosmed/add') ?>" role="form" enctype="multipart/form-data">
                                    <label>Username Social Media</label>
                                    <div class="mb-3">
                                        <input type="text" class="form-control border-danger" name="name" id="floatingInput" placeholder="Username Social Media">
                                    </div>

                                    <label>URL</label>
                                    <div class="mb-3">
                                        <input type="text" class="form-control border-danger" name="url" id="floatingInput" placeholder="URL">
                                    </div>

                                    <label>Icon</label>
                                    <div class="mb-3">
                                        <input type="file" class="form-control border-danger" name="icon" id="floatingInput" size="20" placeholder="Icon" accept="image/x-png, image/jpeg, image/jpg">
                                    </div>
                                    
                                    <div class="ms-2">
                                        <button type="submit" class="btn bg-gradient-info w-25 mt-4 mb-0">Add</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        <div class="container-fluid py-4">
            <div class="row my-4">
                <?= $this->session->flashdata('message'); ?>
                <div class="col-lg-12 col-md-8 mb-md-0 mb-4">
                    <div class="card">
                        <div class="card-body px-3 pb-4">
                            <div class="table-responsive ps-3">
                                <form method="POST" action="<?= base_url('Dsosmed/edit/') . $sosmed->id ?>" role="form">
                                    <input type="hidden" name="id" value="<?php echo $sosmed->id ?>">

                                    <label>Username Social Media</label>
                                    <div class="mb-3">
                                        <input type="text" class="form-control border-danger" name="name" id="floatingInput" placeholder="Username Social Media" value="<?= $sosmed->name ?>">

                                    </div>

                                    <label>URL</label>
                                    <div class="mb-3">
                                        <input type="text" class="form-control border-danger" name="url" id="floatingInput" placeholder="URL" value="<?= $sosmed->url ?>">
                                    </div>

                                    <label>Icon</label>
                                    <div class="mb-3">
                                        <input type="file" class="form-control border-danger" name="icon" id="floatingInput" placeholder="Icon" value="<?= $sosmed->icon ?>">
                                        <img src="<?= base_url('assets/img/sosmedImage') . "/" . $sosmed->icon ?>" class="img-fluid mt-3 bg-gradient-primary" alt="sosmed icon">

                                        <input type="hidden" name="iconLama" value="<?php echo $sosmed->icon ?>">
                                    </div>

                                    <div class="ms-2">
                                        <button type="submit" class="btn bg-gradient-info w-25 mt-4 mb-0">Update</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
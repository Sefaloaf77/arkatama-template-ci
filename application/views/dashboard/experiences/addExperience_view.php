        <div class="container-fluid py-4">
            <div class="row my-4">
                <?= $this->session->flashdata('message'); ?>
                <div class="col-lg-12 col-md-8 mb-md-0 mb-4">
                    <div class="card">
                        <div class="card-body px-3 pb-4">
                            <div class="table-responsive ps-3">
                                <form method="POST" action="<?= base_url('Dexperience/add') ?>" role="form">
                                
                                    <label>Office Name</label>
                                    <div class="mb-3">
                                        <input type="text" class="form-control border-danger" name="name" id="floatingInput" placeholder="Office Name" value="<?= set_value('name') ?>">
                                        <?= form_error('name', '<small class="text-danger pl-3">', '</small>'); ?>
                                    </div>

                                    <label>Start Year</label>
                                    <div class="mb-3">
                                        <input type="date" class="form-control border-danger" name="start_year" id="floatingInput" placeholder="Start Year" value="<?= set_value('start_year') ?>">

                                        <?= form_error('start_year', '<small class="text-danger pl-3">', '</small>'); ?>
                                    </div>

                                    <label>Resign Date</label>
                                    <div class="mb-3">
                                        <input type="date" class="form-control border-danger" name="resign_date" id="floatingInput" placeholder="Resign Date" value="<?= set_value('resign_date') ?>">

                                        <?= form_error('resign_date', '<small class="text-danger pl-3">', '</small>'); ?>
                                    </div>

                                    <label>Description</label>
                                    <div class="mb-3">
                                        <textarea class="form-control border-danger" name="description" placeholder="Description" cols="10" rows="5" value="<?= set_value('description') ?>"></textarea>
                                        <?= form_error('description', '<small class="text-danger pl-3">', '</small>'); ?>
                                    </div>

                                    <div class="ms-2">
                                        <button type="submit" class="btn bg-gradient-info w-25 mt-4 mb-0">Add</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        <div class="container-fluid py-4">
            <div class="row my-4">
                <?= $this->session->flashdata('message'); ?>
                <div class="col-lg-12 col-md-8 mb-md-0 mb-4">
                    <div class="card">
                        <div class="card-body px-3 pb-4">
                            <div class="table-responsive ps-3">
                                <form method="POST" action="<?= base_url('Dcategory/edit/') . $category->id ?>" role="form">
                                    <label>Category Name</label>
                                    <div class="mb-3">
                                        <input type="text" class="form-control border-danger" name="name" id="floatingInput" placeholder="Category Name" value="<?= $category->name ?>">
                                        <?= form_error('name', '<small class="text-danger pl-3">', '</small>'); ?>
                                    </div>
                                    <div class="ms-2">
                                        <button type="submit" class="btn bg-gradient-info w-25 mt-4 mb-0">Update</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
<?php 
define('BASEPATH') OR exit('No direct script access allowed');

if (! function_exists('dashboard')) {
    function cekHalaman($view){
        $user = $this->db->get_where('users', ['username' => $this->session->userdata('username')])->row_array();
        if ($user != null) {
            $data['user'] = $this->db->get_where('users', ['username' => $this->session->userdata('username')])->row_array();
            $this->load->view('dashboard/layout/header');
            $this->load->view('dashboard/layout/sidebar');
            $this->load->view($view, $data);
            $this->load->view('dashboard/index');
            $this->load->view('dashboard/layout/footer');
        } else {
            redirect('portofolio/home');
        }
    }
}

?>
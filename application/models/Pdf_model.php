<?php
class Pdf_model extends CI_Model
{

    public function getCV()
    {
        $this->db->select([
            "name",
            "value",
        ]);
        return $this->db->get('cv_data')->result_array();
    }
    public function getSosmed()
    {
        $this->db->select([
            "social_medias.name",
            "social_medias.url",
            "social_medias.icon",
        ]);
        return $this->db->get('social_medias')->result();
    }
    public function getSkill()
    {
        $this->db->select([
            "skills.name",
            "skills.level",
        ]);
        return $this->db->get('skills')->result();
    }
    public function getLanguage()
    {
        $this->db->select([
            "languages.name",
            "languages.level",
        ]);
        return $this->db->get('languages')->result();
    }
    public function getHobby()
    {
        $this->db->select([
            "hobbies.hobby",
        ]);
        return $this->db->get('hobbies')->result();
    }
    public function getEdu()
    {
        $this->db->select([
            "educations.name",
            "educations.start_year",
            "educations.graduated_date",
            "educations.description",
        ]);
        return $this->db->get('educations')->result();
    }
    public function getExp()
    {
        $this->db->select([
            "experiences.name",
            "experiences.start_year",
            "experiences.resign_date",
            "experiences.description",
        ]);
        return $this->db->get('experiences')->result();
    }
}
?>
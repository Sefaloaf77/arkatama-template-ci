<?php

class WorkCategory_model extends CI_Model
{
    private $_table = "work_categories";

    public $work_id;
    public $category_id;

    public function rules()
    {
        return [
            [
                'field' => 'work_id',
                'label' => 'work_id',
                'rules' => 'required'
            ],
            [
                'field' => 'category_id',
                'label' => 'category_id',
                'rules' => 'required'
            ],
        ];
    }

    public function getAll()
    {
        return $this->db->get($this->_table)->result();
    }

    public function getById($id)
    {
        return $this->db->get_where($this->_table, ['work_id' => $id])->row();
    }
    public function save($data)
    {
        $this->work_id = $data["work_id"];
        $this->category_id = $data["category_id"];
        return $this->db->insert($this->_table, $this);
    }
    public function update($id, $data)
    {
        $this->work_id = $data["work_id"];
        $this->category_id = $data["category_id"];
        return $this->db->update($this->_table, $this, array("work_id" => $id));
    }
    public function delete($id)
    {
        return $this->db->delete($this->_table, array('work_id' => $id));
    }
}

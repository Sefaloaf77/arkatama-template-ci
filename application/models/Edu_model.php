<?php

class Edu_model extends CI_Model
{
    private $_table = "educations";

    // public $id;
    public $name;
    public $start_year;
    public $graduated_date;
    public $description;

    public function rules()
    {
        return [
            [
                'field' => 'name',
                'label' => 'name',
                'rules' => 'required'
            ],
            [
                'field' => 'start_year',
                'label' => 'start_year',
                'rules' => 'required'
            ],
            [
                'field' => 'graduated_date',
                'label' => 'graduated_date',
                'rules' => 'required'
            ],
            [
                'field' => 'description',
                'label' => 'description',
                'rules' => 'required'
            ]
        ];
    }

    public function getAll()
    {
        return $this->db->get($this->_table)->result();
    }

    public function getById($id)
    {
        return $this->db->get_where($this->_table, ["id" => $id])->row();
    }
    public function save($data)
    {
        $this->name = $data["name"];
        $this->start_year = $data["start_year"];
        $this->graduated_date = $data["graduated_date"];
        $this->description = $data["description"];
        return $this->db->insert($this->_table, $this);
    }
    public function update($id, $data)
    {
        $this->name = $data["name"];
        $this->start_year = $data["start_year"];
        $this->graduated_date = $data["graduated_date"];
        $this->description = $data["description"];
        return $this->db->update($this->_table, $this, array("id" => $id));
    }
    public function delete($id)
    {
        return $this->db->delete($this->_table, array('id' => $id));
    }
}

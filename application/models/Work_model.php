<?php

class Work_model extends CI_Model
{
    protected $_table = "works";

    // public $id;
    public $title;
    public $year;
    public $content;
    public $featured_image;
    public $created_by;
    public $updated_by;

    public function rules()
    {
        return [
            [
                'field' => 'title',
                'label' => 'title',
                'rules' => 'required'
            ],
            [
                'field' => 'year',
                'label' => 'year',
                'rules' => 'required'
            ],
            [
                'field' => 'content',
                'label' => 'content',
                'rules' => 'required'
            ],
            [
                'field' => 'created_by',
                'label' => 'created_by',
                'rules' => 'numeric'
            ],
            [
                'field' => 'updated_by',
                'label' => 'updated_by',
                'rules' => 'numeric'
            ]
        ];
    }

    public function getAll()
    {
        $this->db->select([
            "w.id",
            "w.title",
            "w.year",
            "w.content",
            "w.featured_image",
            "w.created_at",
            "w.updated_at",
            "u.name as created_by",
            "u2.name as updated_by"
        ]);
        $this->db->join("users u", "u.id = w.created_by", "left");
        $this->db->join("users u2", "u2.id = w.updated_by", "left");
        $getAll = $this->db->get($this->_table . ' w')->result();
        return $getAll;
    }

    public function getById($id)
    {
        return $this->db->get_where($this->_table, ["id" => $id])->row();
    }
    public function save($data)
    {
        $this->title = $data["title"];
        $this->year = $data["year"];
        $this->content = $data["content"];
        $this->featured_image = $data["featured_image"];
        $this->created_by = $data["created_by"];
        $this->updated_by = $data["updated_by"];
        $this->db->trans_start();
        return $this->db->insert($this->_table, $this);
        $this->db->trans_complete();
    }
    public function update($id, $data)
    {
        $this->title = $data["title"];
        $this->year = $data["year"];
        $this->content = $data["content"];
        $this->featured_image = $data["featured_image"];
        $this->updated_by = $data["updated_by"];
        $this->db->trans_start();
        return $this->db->update($this->_table, $this, array("id" => $id));
        $this->db->trans_complete();
    }
    public function delete($id)
    {
        return $this->db->delete($this->_table . 'work_categories', array('id' => $id));
        // return $deleteRelasi = $this->db->delete('work_categories', array('id' => $id));
        // return $deleteWork;
        // return $deleteRelasi;
    }
    public function updateFile($id, $data)
    {
        $this->db->trans_start();
        $this->db->where('id', $id);
        return $this->db->update('works', $data);
        $this->db->trans_complete();
    }

    var $column_order = array(null, 'w.title', 'w.year',null, 'w.created_at',null, 'w.updated_at');
    var $order = ['created_at' =>'asc'];
    private function querydatatable($search, $start, $length, $filter)
    {
        $this->db->select([
            "w.id",
            "title",
            "year",
            "featured_image",
            "created_at",
            "u.name as created_by",
            "u2.name as updated_by",
            "updated_at"
        ]);

        if ($search["value"] != "") {
            $this->db->or_like([
                "title"      => $search["value"],
                "content"    => $search["value"]
            ]);
        }
        if ($filter["year"] != "") {
            $this->db->or_like([
                "year" => $filter["year"]
            ]);
        }
        if ($filter["category"] != "") {
            $this->db->or_where_in("category_id", $filter["category"]);
        }
        if(isset($_POST['order'])){
            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } else if (isset($this->order)) {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }

        // $this->db->order_by("created_at asc");
        $this->db->join("users u", "u.id = w.created_by", "left");
        $this->db->join("users u2", "u2.id = w.updated_by", "left");
        $this->db->join("work_categories wc", "wc.work_id = w.id", "left");
        $this->db->group_by("w.id");
        $this->db->limit($length, $start);
        
        $works = $this->db->get($this->_table . " w");
        return $works;
    }

    public function getdatatable($search, $start, $length, $filter)
    {
        $works = $this->querydatatable($search, $start, $length, $filter)->result();
        return $works;
    }
    public function countTotal()
    {
        return $this->db->count_all($this->_table);
    }

    public function countFiltered($search, $start, $length, $filter)
    {
        $works = $this->querydatatable($search, $start, $length, $filter);
        return $works->num_rows();
    }
}

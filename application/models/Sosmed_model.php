<?php

class Sosmed_model extends CI_Model
{
    private $_table = "social_medias";

    // public $id;
    public $name;
    public $url;
    public $icon;

    public function rules()
    {
        return [
            [
                'field' => 'name',
                'label' => 'name',
                'rules' => 'required'
            ],
            [
                'field' => 'url',
                'label' => 'url',
                'rules' => 'required'
            ],
            [
                'field' => 'icon',
                'label' => 'icon',
                'rules' => 'required'
            ]
        ];
    }
    
    public function getAll()
    {
        return $this->db->get($this->_table)->result();
    }

    public function getById($id)
    {
        return $this->db->get_where($this->_table, ["id" => $id])->row();
    }
    public function save($data)
    {
        $this->name = $data["name"];
        $this->url = $data["url"];
        $this->icon = $data["icon"];
        return $this->db->insert($this->_table, $this);
    }
    public function update($id, $data)
    {
        $this->name = $data["name"];
        $this->url = $data["url"];
        $this->icon = $data["icon"];
        return $this->db->update($this->_table, $this, array("id" => $id));
    }
    public function delete($id)
    {
        return $this->db->delete($this->_table, array('id' => $id));
    }
}

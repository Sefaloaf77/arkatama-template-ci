<?php

class Exp_model extends CI_Model
{
    private $_table = "experiences";

    public $name;
    public $start_year;
    public $resign_date;
    public $description;

    public function rules()
    {
        return [
            [
                'field' => 'name',
                'label' => 'name',
                'rules' => 'required'
            ],
            [
                'field' => 'start_year',
                'label' => 'start_year',
                'rules' => 'required'
            ],
            [
                'field' => 'resign_date',
                'label' => 'resign_date',
                'rules' => 'required'
            ],
            [
                'field' => 'description',
                'label' => 'description',
                'rules' => 'required'
            ]
        ];
    }

    public function getAll()
    {
        return $this->db->get($this->_table)->result();
    }

    public function getById($id)
    {
        return $this->db->get_where($this->_table, ["id" => $id])->row();
    }
    public function save($data)
    {
        $this->name = $data["name"];
        $this->start_year = $data["start_year"];
        $this->resign_date = $data["resign_date"];
        $this->description = $data["description"];
        return $this->db->insert($this->_table, $this);
    }
    public function update($id, $data)
    {
        $this->name = $data["name"];
        $this->start_year = $data["start_year"];
        $this->resign_date = $data["resign_date"];
        $this->description = $data["description"];
        return $this->db->update($this->_table, $this, array("id" => $id));
    }
    public function delete($id)
    {
        return $this->db->delete($this->_table, array('id' => $id));
    }
}

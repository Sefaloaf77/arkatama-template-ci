<?php
defined('BASEPATH') or exit('No direct script access allowed');

class AjaxWorks extends CI_Controller
{
    public function index()
    {
        $data['user'] = $this->db->get_where('users', ['username' => $this->session->userdata('username')])->row_array();
        if ($data != null) {
            // $data['user'] = $this->db->get_where('users', ['username' => $this->session->userdata('username')])->row_array();
            $this->load->view('dashboard/layout/header');
            $this->load->view('dashboard/layout/sidebar');
            $this->load->view('dashboard/layout/navbar', $data);

            $data['works'] = $this->Work_model->getAll();
            $this->load->view('dashboard/works/work_view', $data);
            $this->load->view('dashboard/layout/footer');
        } else {
            redirect('');
        }
    }
    
}

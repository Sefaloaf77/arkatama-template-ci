<?php

class Dpost extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->library('session');
        $this->load->library('form_validation');
        $this->load->model("Post_model");
    }
    public function index()
    {
        $this->load->library('session');
        $data['user'] = $this->db->get_where('users', ['username' => $this->session->userdata('username')])->row_array();

        if ($data != null) {
            $this->load->view('dashboard/layout/header');
            $this->load->view('dashboard/layout/sidebar');
            $this->load->view('dashboard/layout/navbar', $data);
            $data['posts'] = $this->Post_model->getAll();
            $data['year'] = $this->db->select('posts.year')->group_by('posts.year')->get('posts')->result();
            $data['category'] = $this->db->query('SELECT * from categories')->result_array();
            $this->load->view('dashboard/posts/post_view',$data);
            $this->load->view('dashboard/layout/footer');
        } else {
            redirect('');
        }
    }
    public function addView()
    {
        $data['user'] = $this->db->get_where('users', ['username' => $this->session->userdata('username')])->row_array();

        $this->load->view('dashboard/layout/header');
        $this->load->view('dashboard/layout/sidebar');
        $this->load->view('dashboard/layout/navbar', $data);
        $data['category'] = $this->db->query('SELECT * from categories')->result_array();
        $this->load->view('dashboard/posts/addPost_view', $data);
        $this->load->view('dashboard/layout/footer');
    }
    public function add()
    {
        $post = $this->Post_model;
        $title = $this->input->post('title');
        $year = $this->input->post('year');
        $content = $this->input->post('content');
        $category_id = $this->input->post('category_id');
        date_default_timezone_set('Asia/Jakarta');
        $timenow = date('d-m-y');
        $ext = $_FILES['featured_image']['name'];
        $config['file_name'] = 'Image-' . $title . '_' . $timenow;
        $config['upload_path'] = './assets/img/postsImage';
        $config['allowed_types'] = 'gif|jpg|png|jpeg';

        $this->load->library('upload', $config);
        $this->upload->do_upload('featured_image');
        $getName = $this->upload->data();
        $gambar = $getName['file_name'];


        $idUSer = $this->db->get_where('users', ['name' => $this->session->userdata('name')])->row_array();
        // $id = $this->session->userdata('id');
        // var_dump($idUSer["id"]);
        // die;

        $validation = $this->form_validation->set_rules($post->rules());
        if ($validation->run()) {
            $data = array(
                'title' => $title,
                'year' => $year,
                'content' => $content,
                'featured_image' => $gambar,
                'created_by' => $idUSer['id'],
                'updated_by' => $idUSer['id'],
            );
            // $post->save($data);
            $this->db->trans_start();
            $this->db->insert('posts', $data);

            $last_id = $this->db->insert_id();
            foreach ($category_id as $category) {
                $data2 = [
                    'post_id' => $last_id,
                    'category_id' => $category
                ];
                $this->db->insert('post_categories', $data2);
                $this->db->trans_complete();
            }

            // var_dump($data2);
            // die;

            $this->session->set_flashdata('message', '<div class="alert alert-success text-light" role="alert">Data added successfully</div>');
            redirect('Dpost');
        } else {
            $this->session->set_flashdata('message', '<div class="alert alert-danger text-light" role="alert">Failed to add data</div>');
            redirect('Dpost/addView');
        }

        // $this->db->insert('posts', $data);

    }
    public function editView($id)
    {
        $data['user'] = $this->db->get_where('users', ['username' => $this->session->userdata('username')])->row_array();
        $data['post'] = $this->Post_model->getById($id);
        $data['category'] = $this->db->query('SELECT * from categories')->result_array();
        $this->load->view('dashboard/layout/header');
        $this->load->view('dashboard/layout/sidebar');
        $this->load->view('dashboard/layout/navbar', $data);
        $this->load->view('dashboard/posts/editPost_view', $data);
        $this->load->view('dashboard/layout/footer');
    }
    public function edit($id = null)
    {
        if (!isset($id)) {

            redirect('Dpost');
        }
        $id = $this->input->post('id');
        $created_by = $this->input->post('created_by');
        $post = $this->Post_model;
        $title = $this->input->post('title');
        $year = $this->input->post('year');
        $content = $this->input->post('content');

        date_default_timezone_set('Asia/Jakarta');
        $timenow = date('d-m-y');
        // $ext = $config['file_ext'] = '.png';
        $config['file_name'] = $this->input->post('gambarLama');
        $config['upload_path'] = './assets/img/postsImage';
        $config['allowed_types'] = 'gif|jpg|png|jpeg';

        $this->load->library('upload', $config);
        $this->upload->do_upload('featured_image');
        $getName = $this->upload->data();
        $gambar = $getName['file_name'];

        if ($gambar == "") {
            $gambar = $this->input->post('gambarLama');
        }

        $idUSer = $this->db->get_where('users', ['name' => $this->session->userdata('name')])->row_array();
        // var_dump($idUSer);
        // die;
        $validation = $this->form_validation->set_rules($post->rules());
        if ($validation->run()) {
            $data = array(
                'id' => $id,
                'title' => $title,
                'year' => $year,
                'content' => $content,
                'featured_image' => $gambar,
                'created_by' => $created_by,
                'updated_by' => $idUSer['id'],
            );
            var_dump($data);
            die;
            // $post->save($data);
            // $this->db->trans_start();
            // $post->update($id, $data);
            $this->db->update('posts', $data, ['id' => $id]);
            // $this->db->trans_complete();
            $this->session->set_flashdata('message', '<div class="alert alert-success text-light" role="alert">Data edited successfully</div>');
            redirect('Dpost');
        } else {
            $this->session->set_flashdata('message', '<div class="alert alert-danger text-light" role="alert">Failed to edit data</div>');
            redirect('Dpost/editView/' . $id);
        }
    }

    public function delete($id = null)
    {
        if (!isset($id)) show_404();

        if ($this->Post_model->delete($id)) redirect('Dpost');
    }
    public function detailView($id)
    {
        $data['user'] = $this->db->get_where('users', ['username' => $this->session->userdata('username')])->row_array();
        $data['post'] = $this->Post_model->getById($id);
        $this->load->view('dashboard/layout/header');
        $this->load->view('dashboard/layout/sidebar');
        $this->load->view('dashboard/layout/navbar', $data);
        $this->load->view('dashboard/posts/detailPost_view', $data);
        $this->load->view('dashboard/layout/footer');
    }
    public function postsAjax()
    {
        $search = $this->input->post("search");
        $draw  = intval($this->input->post("draw"));
        $start  = intval($this->input->post("start"));
        $length  = intval($this->input->post("length"));
        $filter = [
            "year" => $this->input->post("year"),
            "category" => $this->input->post("category")
        ];
        $posts = $this->Post_model->getdatatable($search, $start, $length,$filter);
        $no = $start + 1;

        foreach ($posts as $i => $post) {
            $post->no = $no++;
        }

        $countAll = $this->Post_model->countTotal();
        $countFiltered = $this->Post_model->countFiltered($search, $start, $length, $filter);

        return $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode([
                "draw"                => $draw,
                "recordsTotal"        => $countAll,
                "recordsFiltered"    => $countFiltered,
                "data"                => $posts
            ]));
    }
}

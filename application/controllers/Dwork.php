<?php

class Dwork extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->library('session');
        $this->load->library('form_validation');
        $this->load->model("Work_model");
    }
    public function index()
    {
        $data['user'] = $this->db->get_where('users', ['username' => $this->session->userdata('username')])->row_array();
        if ($data != null) {
            // $data['user'] = $this->db->get_where('users', ['username' => $this->session->userdata('username')])->row_array();
            $this->load->view('dashboard/layout/header');
            $this->load->view('dashboard/layout/sidebar');
            $this->load->view('dashboard/layout/navbar', $data);

            $data['works'] = $this->Work_model->getAll();
            $data['year'] = $this->db->select('works.year')->group_by('works.year')->get('works')->result();
            $data['category'] = $this->db->query('SELECT * from categories')->result_array();
            $this->load->view('dashboard/works/work_view', $data);
            $this->load->view('dashboard/layout/footer');
        } else {
            redirect('');
        }
    }
    public function addView()
    {
        $data['user'] = $this->db->get_where('users', ['username' => $this->session->userdata('username')])->row_array();

        $this->load->view('dashboard/layout/header');
        $this->load->view('dashboard/layout/sidebar');
        $this->load->view('dashboard/layout/navbar', $data);
        $data['category'] = $this->db->query('SELECT * from categories')->result_array();
        $this->load->view('dashboard/works/addWork_view', $data);
        $this->load->view('dashboard/layout/footer');
    }
    
    public function add()
    {
        $work = $this->Work_model;
        $title = $this->input->post('title');
        $year = $this->input->post('year');
        $content = $this->input->post('content');
        $category_id = $this->input->post('category_id');
        date_default_timezone_set('Asia/Jakarta');
        $timenow = date('d-m-y');
        $ext = $_FILES['featured_image']['name'];
        $config['file_name'] = 'Image-' . $title . '_' . $timenow;
        $config['upload_path'] = './assets/img/worksImage';
        $config['allowed_types'] = 'gif|jpg|png|jpeg';

        $this->load->library('upload', $config);
        $this->upload->do_upload('featured_image');
        $getName = $this->upload->data();
        $gambar = $getName['file_name'];

        $idUSer = $this->db->get_where('users', ['name' => $this->session->userdata('name')])->row_array();

        $validation = $this->form_validation->set_rules($work->rules());
        if ($validation->run()) {
            $data = array(
                'title' => $title,
                'year' => $year,
                'content' => $content,
                'featured_image' => $gambar,
                'created_by' => $idUSer['id'],
                'updated_by' => $idUSer['id'],
            );
            // $work->save($data);
            $this->db->trans_start();
            $this->db->insert('works', $data);

            $last_id = $this->db->insert_id();
            foreach ($category_id as $category) {
                $data2 = [
                    'work_id' => $last_id,
                    'category_id' => $category
                ];
                $this->db->insert('work_categories', $data2);
            }
            $this->db->trans_complete();
            $this->session->set_flashdata('message', '<div class="alert alert-success text-light" role="alert">Data added successfully</div>');
            redirect('Dwork');
        } else {
            $this->session->set_flashdata('message', '<div class="alert alert-danger text-light" role="alert">Failed to add data</div>');
            redirect('Dwork/addView');
        }

        // $this->db->insert('works', $data);

    }
    public function editView($id)
    {
        $data['user'] = $this->db->get_where('users', ['username' => $this->session->userdata('username')])->row_array();
        $data['work'] = $this->Work_model->getById($id);
        $data['category'] = $this->db->query('SELECT * from categories')->result_array();
        $this->load->view('dashboard/layout/header');
        $this->load->view('dashboard/layout/sidebar');
        $this->load->view('dashboard/layout/navbar', $data);
        $this->load->view('dashboard/works/editWork_view', $data);
        $this->load->view('dashboard/layout/footer');
    }
    public function edit($id=null)
    {
        if (!isset($id)) {

            redirect('Dwork');
        }
        $id = $this->input->post('id');
        $created_by = $this->input->post('created_by');
        $work = $this->Work_model;
        $title = $this->input->post('title');
        $year = $this->input->post('year');
        $content = $this->input->post('content');

        date_default_timezone_set('Asia/Jakarta');
        $timenow = date('d-m-y');
        // $ext = $config['file_ext'] = '.png';
        $config['file_name'] = $this->input->post('gambarLama');
        $config['upload_path'] = './assets/img/worksImage';
        $config['allowed_types'] = 'gif|jpg|png|jpeg';

        $this->load->library('upload', $config);
        $this->upload->do_upload('featured_image');
        $getName = $this->upload->data();
        $gambar = $getName['file_name'];
        
        if ($gambar == "") {
            $gambar = $this->input->post('gambarLama');
        }
        
        // var_dump($category_id);
        // die;
        $idUSer = $this->db->get_where('users', ['name' => $this->session->userdata('name')])->row_array();

        $validation = $this->form_validation->set_rules($work->rules());
        if ($validation->run()) {
            $data = array(
                'title' => $title,
                'year' => $year,
                'content' => $content,
                'featured_image' => $gambar,
                'created_by' => $created_by,
                'updated_by' => $idUSer['id'],
            );
            // var_dump($data);
            // die;
            // $work->save($data);
            $this->db->trans_start();
            $this->db->update('works', $data, ['id' => $id]);
            $this->db->trans_complete();
            $this->session->set_flashdata('message', '<div class="alert alert-success text-light" role="alert">Data edited successfully</div>');
            redirect('Dwork');
        } else {
            $this->session->set_flashdata('message', '<div class="alert alert-danger text-light" role="alert">Failed to edit data</div>');
            redirect('Dwork/editView/' . $id);
        }
    }
    public function delete($id = null)
    {
        if (!isset($id)) show_404();

        if ($this->Work_model->delete($id)) redirect('Dwork');
    }
    public function detailView($id)
    {
        $data['user'] = $this->db->get_where('users', ['username' => $this->session->userdata('username')])->row_array();
        $data['work'] = $this->Work_model->getById($id);
        $this->load->view('dashboard/layout/header');
        $this->load->view('dashboard/layout/sidebar');
        $this->load->view('dashboard/layout/navbar', $data);
        $this->load->view('dashboard/works/detailWork_view', $data);
        $this->load->view('dashboard/layout/footer');
    }
    public function worksAjax()
    {
        $search = $this->input->post("search");
        $draw  = intval($this->input->post("draw"));
        $start  = intval($this->input->post("start"));
        $length  = intval($this->input->post("length"));
        // var_dump($this->input->post("year"));
        // die;
        $filter = [
            "year" => $this->input->post("year"),
            "category" =>$this->input->post("category")
        ];
        $works = $this->Work_model->getdatatable($search, $start, $length, $filter);
        $no = $start + 1;

        foreach ($works as $i => $work) {
            $work->no = $no++;
        }

        $countAll = $this->Work_model->countTotal();
        $countFiltered = $this->Work_model->countFiltered($search, $start, $length, $filter);

        return $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode([
                "draw"                => $draw,
                "recordsTotal"        => $countAll,
                "recordsFiltered"    => $countFiltered,
                "data"                => $works
            ]));
    }
}

<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Dexperience extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->library('session');
        $this->load->library('form_validation');
        $this->load->model("Exp_model");
    }
    public function index()
    {
        $data['user'] = $this->db->get_where('users', ['username' => $this->session->userdata('username')])->row_array();
        if ($data != null) {
            $this->load->view('dashboard/layout/header');
            $this->load->view('dashboard/layout/sidebar');
            $this->load->view('dashboard/layout/navbar', $data);

            $data['exp'] = $this->Exp_model->getAll();
            $this->load->view('dashboard/experiences/experience_view', $data);
            $this->load->view('dashboard/layout/footer');
        } else {
            redirect('');
        }
    }

    public function addView()
    {
        $data['user'] = $this->db->get_where('users', ['username' => $this->session->userdata('username')])->row_array();
        $this->load->view('dashboard/layout/header');
        $this->load->view('dashboard/layout/sidebar');
        $this->load->view('dashboard/layout/navbar', $data);
        $this->load->view('dashboard/experiences/addExperience_view');
        $this->load->view('dashboard/layout/footer');
    }
    public function add()
    {
        $exp = $this->Exp_model;
        $validation = $this->form_validation->set_rules($exp->rules());
        if ($validation->run()) {
            $data = $this->input->post();
            $exp->save($data);
            $this->session->set_flashdata('message', '<div class="alert alert-success text-light" role="alert">Data added successfully</div>');
            redirect('Dexperience');
        } else {
            $this->session->set_flashdata('message', '<div class="alert alert-danger text-light" role="alert">Failed to add data</div>');
            redirect('Dexperience/addView');
        }
    }

    public function editView($id)
    {
        $data['user'] = $this->db->get_where('users', ['username' => $this->session->userdata('username')])->row_array();
        $data['exp'] = $this->Exp_model->getById($id);
        $this->load->view('dashboard/layout/header');
        $this->load->view('dashboard/layout/sidebar');
        $this->load->view('dashboard/layout/navbar', $data);
        $this->load->view('dashboard/experiences/editExperience_view', $data);
        $this->load->view('dashboard/layout/footer');
    }
    public function edit($id = null)
    {
        if (!isset($id)) {

            redirect('Dexperience');
        }

        $exp = $this->Exp_model;
        $validation = $this->form_validation->set_rules($exp->rules());
        if ($validation->run()) {
            $data = $this->input->post();
            // var_dump($data);
            // die;
            $exp->update($id, $data);

            $this->session->set_flashdata('message', '<div class="alert alert-success text-light" role="alert">Data edited successfully</div>');
            redirect('Dexperience');
        } else {
            $this->session->set_flashdata('message', '<div class="alert alert-danger text-light" role="alert">Failed to edit data</div>');
            redirect('Dexperience/editView');
        }
    }

    public function delete($id = null)
    {
        if (!isset($id)) show_404();

        if ($this->Exp_model->delete($id)) redirect('Dexperience');
    }
}

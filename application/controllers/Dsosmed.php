<?php

class Dsosmed extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->library('session');
        $this->load->library('form_validation');
        $this->load->model("Sosmed_model");
    }
    public function index()
    {
        $data['user'] = $this->db->get_where('users', ['username' => $this->session->userdata('username')])->row_array();
        if ($data != null) {
            // $data['user'] = $this->db->get_where('users', ['username' => $this->session->userdata('username')])->row_array();
            $this->load->view('dashboard/layout/header');
            $this->load->view('dashboard/layout/sidebar');
            $this->load->view('dashboard/layout/navbar', $data);

            $data['sosmed'] = $this->Sosmed_model->getAll();
            $this->load->view('dashboard/sosmed/sosmed_view', $data);
            $this->load->view('dashboard/layout/footer');
        } else {
            redirect('');
        }
    }
    public function addView()
    {
        $data['user'] = $this->db->get_where('users', ['username' => $this->session->userdata('username')])->row_array();

        $this->load->view('dashboard/layout/header');
        $this->load->view('dashboard/layout/sidebar');
        $this->load->view('dashboard/layout/navbar', $data);
        $this->load->view('dashboard/sosmed/addSosmed_view', $data);
        $this->load->view('dashboard/layout/footer');
    }
    public function add()
    {
        $sosmed = $this->Sosmed_model;
        $name = $this->input->post('name');
        $url = $this->input->post('url');
        
        date_default_timezone_set('Asia/Jakarta');
        $timenow = date('d-m-y');
        $ext = $_FILES['icon']['name'];
        $config['file_name'] = 'Image-' . $name . '_' . $timenow;
        $config['upload_path'] = './assets/img/sosmedImage';
        $config['allowed_types'] = 'gif|jpg|png|jpeg';

        $this->load->library('upload', $config);
        // $this->upload->initialize($config);
        $this->upload->do_upload('icon');
        $getName = $this->upload->data();
        $icon = $getName['file_name'];
        
        // $validation = $this->form_validation->set_rules($sosmed->rules());
        // var_dump($icon);
        // die;
        if ($name !=null && $url !=null && $icon !=null) {
            $data = array(
                'name' => $name,
                'url' => $url,
                'icon' => $icon,
            );
            
            $sosmed->save($data);
            // $this->db->trans_start();
            // $this->db->insert('social_medias', $data);
            // $this->db->trans_complete();
            $this->session->set_flashdata('message', '<div class="alert alert-success text-light" role="alert">Data added successfully</div>');
            redirect('Dsosmed');
        } else {
            $this->session->set_flashdata('message', '<div class="alert alert-danger text-light" role="alert">Failed to add data</div>');
            redirect('Dsosmed/addView');
        }

        // $this->db->insert('works', $data);
    }
    public function editView($id)
    {
        $data['user'] = $this->db->get_where('users', ['username' => $this->session->userdata('username')])->row_array();
        $data['sosmed'] = $this->Sosmed_model->getById($id);
        $this->load->view('dashboard/layout/header');
        $this->load->view('dashboard/layout/sidebar');
        $this->load->view('dashboard/layout/navbar', $data);
        $this->load->view('dashboard/sosmed/editSosmed_view', $data);
        $this->load->view('dashboard/layout/footer');
    }
    public function edit($id=null)
    {
        if (!isset($id)) {

            redirect('Dsosmed');
        }
        $id = $this->input->post('id');
        $sosmed = $this->Sosmed_model;
        $name = $this->input->post('name');
        $url = $this->input->post('url');
        
        date_default_timezone_set('Asia/Jakarta');
        $timenow = date('d-m-y');
        // $ext = $_FILES['image']['name'];
        $config['file_ext'] = '.png';
        $config['file_type'] = 'image/png';
        $config['image_type'] = 'gif|jpg|png|jpeg';
        $config['file_name'] = $this->input->post('icon');
        $config['upload_path'] = './assets/img/sosmedImage';
        $config['allowed_types'] = 'gif|jpg|png|jpeg';

        $this->load->library('upload', $config);
        $this->upload->do_upload('icon');
        $getName = $this->upload->data();
        $icon = $getName['file_name'];
        
        if ($icon == "") {
            $icon = $this->input->post('iconLama');
        }
        
        // var_dump($icon);
        // die;
        // $idUSer = $this->db->get_where('users', ['name' => $this->session->userdata('name')])->row_array();

        $validation = $this->form_validation->set_rules($sosmed->rules());
        if ($name !=null && $url !=null && $icon !=null) {
            $data = array(
                'name' => $name,
                'url' => $url,
                'icon' => $icon,
            );
            // var_dump($data);
            // die;
            // $work->save($data);
            $this->db->trans_start();
            $this->db->update('social_medias', $data, ['id' => $id]);
            $this->db->trans_complete();
            $this->session->set_flashdata('message', '<div class="alert alert-success text-light" role="alert">Data edited successfully</div>');
            redirect('Dsosmed');
        } else {
            $this->session->set_flashdata('message', '<div class="alert alert-danger text-light" role="alert">Failed to edit data</div>');
            redirect('Dsosmed/editView/' . $id);
        }
    }
    public function delete($id = null)
    {
        if (!isset($id)) show_404();

        if ($this->Sosmed_model->delete($id)) redirect('Dsosmed');
    }
}

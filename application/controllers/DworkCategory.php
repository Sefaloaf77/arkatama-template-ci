<?php

class DworkCategory extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->library('session');
        $this->load->library('form_validation');
        $this->load->model("WorkCategory_model");
    }
    public function index()
    {
        $this->load->library('session');
        $data['user'] = $this->db->get_where('users', ['username' => $this->session->userdata('username')])->row_array();
        if ($data != null) {
            // $data['user'] = $this->db->get_where('users', ['username' => $this->session->userdata('username')])->row_array();
            $this->load->view('dashboard/layout/header');
            $this->load->view('dashboard/layout/sidebar');
            $this->load->view('dashboard/layout/navbar', $data);

            $data['workCategories'] = $this->WorkCategory_model->getAll();
            $this->load->view('dashboard/workCategory/Wcategory_view',$data);
            $this->load->view('dashboard/layout/footer');
        } else {
            redirect('');
        }
    }
    public function addView()
    {
        $data['user'] = $this->db->get_where('users', ['username' => $this->session->userdata('username')])->row_array();
        $this->load->view('dashboard/layout/header');
        $this->load->view('dashboard/layout/sidebar');
        $this->load->view('dashboard/layout/navbar', $data);
        $this->load->view('dashboard/workCategory/add_view');
        $this->load->view('dashboard/layout/footer');
    }
    public function add()
    {
        $workCategory = $this->WorkCategory_model;
        $validation = $this->form_validation->set_rules($workCategory->rules());
        if ($validation->run()) {
            $data = $this->input->post();
            $workCategory->save($data);
            $this->session->set_flashdata('message', '<div class="alert alert-success text-light" role="alert">Data added successfully</div>');
            redirect('DworkCategory');
        } else {
            $this->session->set_flashdata('message', '<div class="alert alert-danger text-light" role="alert">Failed to add data</div>');
            redirect('DworkCategory/addView');
        }
        // $this->load->view('dashboard/category/addCategory_view');
    }
    public function editView($id)
    {
        $data['user'] = $this->db->get_where('users', ['username' => $this->session->userdata('username')])->row_array();
        $data['workCategories'] = $this->WorkCategory_model->getById($id);
        $this->load->view('dashboard/layout/header');
        $this->load->view('dashboard/layout/sidebar');
        $this->load->view('dashboard/layout/navbar', $data);
        $this->load->view('dashboard/workCategory/edit_view', $data);
        $this->load->view('dashboard/layout/footer');
    }
    public function edit($id = null)
    {
        if (!isset($id)) {

            redirect('DworkCategory');
        }

        $workCategory = $this->WorkCategory_model;
        $validation = $this->form_validation->set_rules($workCategory->rules());
        if ($validation->run()) {
            $data = $this->input->post();
            // var_dump($data);
            // die;
            $workCategory->update($id, $data);

            $this->session->set_flashdata('message', '<div class="alert alert-success text-light" role="alert">Data edited successfully</div>');
            redirect('DworkCategory');
        } else {
            $this->session->set_flashdata('message', '<div class="alert alert-danger text-light" role="alert">Failed to edit data</div>');
            redirect('DworkCategory/editView');
        }
        $data['workCategories'] = $workCategory->getById($id);

        // $this->load->view('dashboard/category/editCategory_view', $data);
    }

    public function delete($id = null)
    {
        if (!isset($id)) show_404();

        if ($this->WorkCategory_model->delete($id)) redirect('DworkCategory');
    }
}

<?php

class Dashboard extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->library('session');
    }
    public function index(){
        // $user = $this->db->get_where('users', ['username' => $this->session->userdata('username')])->row_array();
        $data['user'] = $this->db->get_where('users', ['username' => $this->session->userdata('username')])->row_array();
        if($data != null){
            // $data['user'] = $this->db->get_where('users', ['username' => $this->session->userdata('username')])->row_array();
            $this->load->view('dashboard/layout/header');
            $this->load->view('dashboard/layout/sidebar');
            $this->load->view('dashboard/layout/navbar', $data);
            $this->load->view('dashboard/index');
            $this->load->view('dashboard/layout/footer');
        } else{
            redirect('/');
        }
    }
}

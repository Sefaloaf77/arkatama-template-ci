<?php

class Auth extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->library('form_validation');
        $this->load->library('session');
    }
    public function index()
    {
        $this->form_validation->set_rules('username', 'Username', 'trim|required');
        $this->form_validation->set_rules('password', 'Password', 'trim|required');

        if ($this->form_validation->run() == false) {
            $data['judul'] = 'Login | Portofolio Web';
            $this->load->view('auth/login_view', $data);
        } else {
            $this->_login();
        }
    }
    private function _login()
    {
        $username = $this->input->post('username');
        $password = $this->input->post('password');

        $user = $this->db->get_where('users', ['username' => $username])->row_array();
        $hashPass = hash('sha256', $password);
        // var_dump($user);
        // die;
        if ($user != null) {
            if ($hashPass == $user['password']) {
                $data = [
                    'username' => $user['username'],
                    'name' => $user['name'],
                    'pasword' => $user['pasword']

                ];
                $this->session->set_userdata($data);
                // var_dump($data);
                // die;
                redirect('dashboard');
            } else {
                $this->session->set_flashdata('message', '<div class="alert alert-danger text-light" role="alert">Username atau Password belum terdaftar</div>');
                redirect('Auth');
            }
        } else {
            $this->session->set_flashdata('message', '<div class="alert alert-danger text-light" role="alert">Username atau Password belum terdaftar</div>');
            redirect('Auth');
        }
    }

    public function registrasi()
    {
        $this->form_validation->set_rules('username', 'Username', 'required|trim|is_unique[users.username]');
        $this->form_validation->set_rules('name', 'Name', 'required');
        $this->form_validation->set_rules('password', 'Password', 'required|trim|min_length[3]');

        if ($this->form_validation->run() == false) {
            $data['judul'] = 'Registrasi | Portofolio Web';
            $this->load->view('auth/register_view', $data);
        } else {
            $data = [
                'username' => $this->input->post('username', true),
                'name' => $this->input->post('name', true),
                'password' => hash('sha256', $this->input->post('password')),
            ];

            $this->db->insert('users', $data);
            $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">Selamat, akunmu telah terdaftar! Mohon login untuk masuk</div>');
            redirect('Auth');
        }
    }

    public function logout()
    {
        $this->session->unset_userdata('username');
        $this->session->unset_userdata('name');
        $this->session->unset_userdata('password');

        $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">Anda berhasil logout</div>');
        redirect('/');
    }
}

<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Deducation extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->library('session');
        $this->load->library('form_validation');
        $this->load->model("Edu_model");
    }
    public function index()
    {
        $data['user'] = $this->db->get_where('users', ['username' => $this->session->userdata('username')])->row_array();
        if ($data != null) {
            $this->load->view('dashboard/layout/header');
            $this->load->view('dashboard/layout/sidebar');
            $this->load->view('dashboard/layout/navbar', $data);

            $data['edu'] = $this->Edu_model->getAll();
            $this->load->view('dashboard/educations/education_view', $data);
            $this->load->view('dashboard/layout/footer');
        } else {
            redirect('');
        }
    }
    public function addView()
    {
        $data['user'] = $this->db->get_where('users', ['username' => $this->session->userdata('username')])->row_array();
        $this->load->view('dashboard/layout/header');
        $this->load->view('dashboard/layout/sidebar');
        $this->load->view('dashboard/layout/navbar', $data);
        $this->load->view('dashboard/educations/addEducation_view');
        $this->load->view('dashboard/layout/footer');
    }
    public function add()
    {
        $education = $this->Edu_model;
        $validation = $this->form_validation->set_rules($education->rules());
        if ($validation->run()) {
            $data = $this->input->post();
            $education->save($data);
            $this->session->set_flashdata('message', '<div class="alert alert-success text-light" role="alert">Data added successfully</div>');
            redirect('Deducation');
        } else {
            $this->session->set_flashdata('message', '<div class="alert alert-danger text-light" role="alert">Failed to add data</div>');
            redirect('Deducation/addView');
        }
        // $this->load->view('dashboard/category/addCategory_view');
    }

    public function editView($id)
    {
        $data['user'] = $this->db->get_where('users', ['username' => $this->session->userdata('username')])->row_array();
        $data['edu'] = $this->Edu_model->getById($id);
        $this->load->view('dashboard/layout/header');
        $this->load->view('dashboard/layout/sidebar');
        $this->load->view('dashboard/layout/navbar', $data);
        $this->load->view('dashboard/educations/editEducation_view', $data);
        $this->load->view('dashboard/layout/footer');
    }
    public function edit($id = null)
    {
        if (!isset($id)) {

            redirect('Deducation');
        }

        $education = $this->Edu_model;
        $validation = $this->form_validation->set_rules($education->rules());
        if ($validation->run()) {
            $data = $this->input->post();
            // var_dump($data);
            // die;
            $education->update($id, $data);

            $this->session->set_flashdata('message', '<div class="alert alert-success text-light" role="alert">Data edited successfully</div>');
            redirect('Deducation');
        } else {
            $this->session->set_flashdata('message', '<div class="alert alert-danger text-light" role="alert">Failed to edit data</div>');
            redirect('Deducation/editView');
        }
        // $data['edu'] = $education->getById($id);

        // $this->load->view('dashboard/category/editCategory_view', $data);
    }

    public function delete($id = null)
    {
        if (!isset($id)) show_404();

        if ($this->Edu_model->delete($id)) redirect('Deducation');
    }
}

<?php

use Dompdf\Dompdf;
use Dompdf\Options;

class Portofolio extends CI_Controller
{
    public function home()
    {
        $this->load->library('session');
        $data['user'] = $this->db->get_where('users', ['username' => $this->session->userdata('username')])->row_array();
        $this->load->view('home/index', $data);
    }
    public function blog()
    {
        $this->load->library('session');
        $data['user'] = $this->db->get_where('users', ['username' => $this->session->userdata('username')])->row_array();
        $this->load->view('home/blog', $data);
    }
    public function works()
    {
        $this->load->library('session');
        $data['user'] = $this->db->get_where('users', ['username' => $this->session->userdata('username')])->row_array();
        $this->load->view('home/works', $data);
    }
    public function WorkDetails()
    {
        $this->load->library('session');
        $data['user'] = $this->db->get_where('users', ['username' => $this->session->userdata('username')])->row_array();
        $this->load->view('home/work-details', $data);
    }
    public function exportPdf()
    {
        $options = new Options();
        $options->setIsRemoteEnabled(TRUE);
        $options->setIsPhpEnabled(TRUE);
        $dompdf = new Dompdf($options);

        $this->load->model("Pdf_model");

        $data['cv_data'] = $this->Pdf_model->getCV();
        // var_dump($data['cv_data'][2]['value']);
        // die;
        $data['educations'] = $this->Pdf_model->getEdu();
        $data['experiences'] = $this->Pdf_model->getExp();
        $data['hobbies'] = $this->Pdf_model->getHobby();
        $data['languages'] = $this->Pdf_model->getLanguage();
        $data['skills'] = $this->Pdf_model->getSkill();
        $data['social_medias'] = $this->Pdf_model->getSosmed();
        
        $view = $this->load->view('CV_portofolio', $data, TRUE);

        // $dompdf->setOptions('isRemoteEnabled', TRUE);

        // $dompdf->nativeFonts('helvetica');
        $dompdf->loadHtml($view);

        // (Optional) Setup the paper size and orientation
        $dompdf->setPaper('A4', 'potrait');

        // Render the HTML as PDF
        $dompdf->render();

        // Output the generated PDF to Browser
        $dompdf->stream("CV_portofolio.pdf", ['Attachment' => false]);
    }
    // public function getDataPDF()
    // {


    //     $this->load->view('CV_portofolio', $data);
    // }
}

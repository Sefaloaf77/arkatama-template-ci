<?php

class Dcategory extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->library('session');
        $this->load->library('form_validation');
        $this->load->model("Category_model");
    }
    public function index()
    {
        $data['user'] = $this->db->get_where('users', ['username' => $this->session->userdata('username')])->row_array();
        if ($data != null) {
            // $data['user'] = $this->db->get_where('users', ['username' => $this->session->userdata('username')])->row_array();
            $this->load->view('dashboard/layout/header');
            $this->load->view('dashboard/layout/sidebar');
            $this->load->view('dashboard/layout/navbar', $data);

            $data['categories'] = $this->Category_model->getAll();
            $this->load->view('dashboard/category/category_view', $data);
            $this->load->view('dashboard/layout/footer');
        } else {
            redirect('');
        }
    }
    public function addView()
    {
        $data['user'] = $this->db->get_where('users', ['username' => $this->session->userdata('username')])->row_array();
        $this->load->view('dashboard/layout/header');
        $this->load->view('dashboard/layout/sidebar');
        $this->load->view('dashboard/layout/navbar', $data);
        $this->load->view('dashboard/category/addCategory_view');
        $this->load->view('dashboard/layout/footer');
    }
    public function add()
    {
        $category = $this->Category_model;
        $validation = $this->form_validation->set_rules($category->rules());
        if ($validation->run()) {
            $data = $this->input->post();
            $category->save($data);
            $this->session->set_flashdata('message', '<div class="alert alert-success text-light" role="alert">Data added successfully</div>');
            redirect('Dcategory');
        } else {
            $this->session->set_flashdata('message', '<div class="alert alert-danger text-light" role="alert">Failed to add data</div>');
            redirect('Dcategory/addView');
        }
        // $this->load->view('dashboard/category/addCategory_view');
    }
    public function editView($id)
    {
        $data['user'] = $this->db->get_where('users', ['username' => $this->session->userdata('username')])->row_array();
        $data['category'] = $this->Category_model->getById($id);
        $this->load->view('dashboard/layout/header');
        $this->load->view('dashboard/layout/sidebar');
        $this->load->view('dashboard/layout/navbar', $data);
        $this->load->view('dashboard/category/editCategory_view', $data);
        $this->load->view('dashboard/layout/footer');
    }
    public function edit($id = null)
    {
        if (!isset($id)) {
            
            redirect('Dcategory');
        }

        $category = $this->Category_model;
        $validation = $this->form_validation->set_rules($category->rules());
        if ($validation->run()) {
            $data = $this->input->post();
            // var_dump($data);
            // die;
            $category->update($id, $data);
            
            $this->session->set_flashdata('message', '<div class="alert alert-success text-light" role="alert">Data edited successfully</div>');
            redirect('Dcategory');
        } else {
            $this->session->set_flashdata('message', '<div class="alert alert-danger text-light" role="alert">Failed to edit data</div>');
            redirect('Dcategory/editView');
        }
        $data['category'] = $category->getById($id);

        // $this->load->view('dashboard/category/editCategory_view', $data);
    }

    public function delete($id = null)
    {
        if (!isset($id)) show_404();

        if ($this->Category_model->delete($id)) redirect('Dcategory');
    }
}

<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require_once("vendor/dompdf/dompdf/autoload.inc.php");
use Dompdf\Dompdf;

/**
* Name:  DOMPDF
* 
* Author: Jd Fiscus
* 	 	  jdfiscus@gmail.com
*         @iamfiscus
*          
*
* Origin API Class: http://code.google.com/p/dompdf/
* 
* Location: http://github.com/iamfiscus/Codeigniter-DOMPDF/
*          
* Created:  06.22.2010 
* 
* Description:  This is a Codeigniter library which allows you to convert HTML to PDF with the DOMPDF library
* 
*/

class Dompdf_gen {
	protected $ci;
		
	public function __construct() {
		$this->ci =& get_instance();
	}
	public function generatePdf($view, $filename = 'CV_portofolio', $paper = 'A4', $orientation='potrait')
	{
		$dompdf = new Dompdf();
		$html = $this->load->view($view, TRUE);
		$dompdf->loadHtml($html);

		// (Optional) Setup the paper size and orientation
		$dompdf->setPaper($paper, $orientation);

		// Render the HTML as PDF
		$dompdf->render();
		$dompdf->stream($filename . ".pdf", array("Attachment" => FALSE));

	}
	
}